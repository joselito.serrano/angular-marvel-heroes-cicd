import { Component } from '@angular/core';
import { Heroes } from 'src/app/Interfaces/heroes';
import { HEROES } from 'src/app/Mock-data/mock-heroes';

@Component({
  selector: 'app-heroes-lists',
  templateUrl: './heroes-lists.component.html',
  styleUrls: ['./heroes-lists.component.css']
})
export class HeroesListsComponent {
  // hero: Heroes = {id:1, name:"Iron Man"};
  // hero = {id:1, name:"Iron Man"} as Heroes;

  heroes = HEROES;
  selectedHero?: Heroes;

  onSelect(myHero: Heroes): void {
    this.selectedHero = myHero;
  }
}
